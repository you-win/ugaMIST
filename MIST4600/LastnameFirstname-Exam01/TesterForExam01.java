/**
 * Tester to print out some Tickets like below
 * 
 * 
 * @author Nikhil Srinivasan
 * @version (a version number or a date)
 */

public class TesterForExam01
{
    // instance variables - replace the example below with your own
    private TicketMachine x;
    private Traveller y;

    /**
     * Constructor for objects of class TesterForExam01
     */
    public TesterForExam01()
    {
        // initialise instance variables
        x = new TicketMachine("Manchester", 5);
        
        x.insertMoney(100);
        System.out.println("**************** Test of PrintTicket***********");
        x.printTicket("Hermoine","Granger",100);
        x.printTicket("Ronald","Weasley",999);
        x.printTicket("Iam","aMuggle",2001);
        System.out.println("The balance in the machine should be 85 pounds and is " + x.getBalance() + " pounds.");
        System.out.println();
        y = new Traveller("Hermoine","Granger",100);
        System.out.println(y.getFname() + " " + y.getLname() + "    " + y.checkIfWizard(100));
        y = new Traveller("Iam","aMuggle",2001);
        System.out.println(y.getFname() + " " + y.getLname() + "    " + y.checkIfWizard(2001));
        System.out.println();        
        System.out.println("****For Part A 1: Expect 2 Wizard tickets and 1 Muggle Ticket *******");
        System.out.println("****For Part A 2: Expect 1 true and 1 false statement, 1 for Hermoine and 1 for Iam ********");

        
        System.out.println();
        x.printTicket("Harry","Potter",232);
        y = new Traveller("Harry","Potter",232);
        System.out.println(y.getFname() + " " + y.getLname() + "    " + y.checkIfHarry(232));
        System.out.println();
        System.out.println("****For Part C: Expect a Ticket saying HP cannot travel and 1 Harry Potter true statement ");
        
    }

}
