
/**
 * The NumberDisplay class represents a digital number display that can hold
 * values from zero to a given limit. The limit can be specified when creating
 * the display. The values range from zero (inclusive) to limit-1. If used,
 * for example, for the seconds on a digital clock, the limit would be 60, 
 * resulting in display values from 0 to 59. When incremented, the display 
 * automatically rolls over to zero when reaching the limit.
 * 
 * @author Michael Kolling and David J. Barnes
 * @version 2006.03.30
 */
public class NumberDisplay
{
    private int limit;
    private int value;
    private int valueA;

    /**
     * Constructor for objects of class NumberDisplay.
     * Set the limit at which the display rolls over.
     */
    public NumberDisplay(int rollOverLimit)
    {
        limit = rollOverLimit;
        value = 0;
        valueA = 0;
    }

    /**
     * Return the current value.
     */
    public int getValue()
    {
        return value;
    }
    public int getAlarmValue(){
        return valueA;
    }

    /**
     * Return the display value (that is, the current value as a two-digit
     * String. If the value is less than ten, it will be padded with a leading
     * zero).
     */
    public String getDisplayValue()
    {
        if(value < 10) {
            return "0" + value;
        }
        else {
            return "" + value;
        }
    }
    private String getAlarmDisplay(){
        if(value < 10) {
            return "0" + valueA;
        }
        else {
            return "" + valueA;
        }
    }

    /**
     * Set the value of the display to the new specified value. If the new
     * value is less than zero or over the limit, do nothing.
     */
    public void setValue(int replacementValue)
    {
        if((replacementValue >= 0) && (replacementValue < limit)) {
            value = replacementValue;
        }
    }

    /**
     * Increment the display value by one, rolling over to zero if the
     * limit is reached.
     */
    public void increment()
    {
        value = (value + 1) % limit;
        // Set value to the remainder of (value+1)/limit.
        // Example 1: (57+1)/60 = 58/60 results in 58
        // Example 2: (58+1)/60 = 59/60 results in 59
        // Example 3: (59+1)/60 = 60/60 results in 0
        // You can write the equivalent of the above with an if else as:
        // if(value+1 >= limit)
        // {
        //     value = 0;
        // }
        // else
        // {
        //     value = value+1;  // Alternate clever way: value++;
	    // } 
    }
}
