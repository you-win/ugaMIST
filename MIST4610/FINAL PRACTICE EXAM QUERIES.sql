-- Practice Final

-- SQL Easy Questions

-- List the required date and order number of all orders shipped in November 2003. Sort by required date and order number.

SELECT requiredDate,orderNumber FROM Orders
	WHERE year(shippedDate) = 2003
    AND month(shippedDate) = 11
    GROUP BY orderNumber
    ORDER BY requiredDate;

-- Report the name (first and last) and email of employees whose job title includes "Manager". Order the report by employee last name.

SELECT emp.firstName,emp.lastName,emp.email FROM Employees emp
	WHERE jobTitle REGEXP "Manager|manager"
    ORDER BY emp.lastName;

-- Which country/countries has/have more than 2 offices?

SELECT country,count(officeCode) as boop FROM Offices
	GROUP BY country HAVING count(officeCode) > 2;

-- List the e-mail and phone extension for all employees working in Australia. Sort results by e-mail.

SELECT emp.email,emp.extension FROM Employees emp,Offices off
	WHERE off.officeCode = emp.officeCode
    AND off.country REGEXP "Australia|australia"
    ORDER BY emp.email;

-- What is the city and country of the southernmost office of Classic Models?

SELECT off.city,off.country FROM Offices off
	WHERE X(off.officeLocation) = (
		SELECT min(X(off.officeLocation)) FROM Offices off
        );

-- SQL Hard Questions

-- Your boss asked you to produce a report for all payments from a customer called La Rochelle Gifts that are 25% or more of the average payments for all customers. 
-- List payment date and the amount.

SELECT pay.paymentDate,pay.amount FROM Payments pay,Customers cust
	WHERE cust.customerNumber = pay.customerNumber
    AND cust.customerName REGEXP "La Rochelle Gifts"
    AND pay.amount >= .25*( -- literally greater than or equal to 25% of the average payments, this is so dumb
		SELECT avg(amount) FROM Payments
        );

-- Produce a report showing the number of products for each order made by Signal Gift Stores (customer name).
-- Sort results so that the order with the most products appears first.

SELECT ord.orderNumber,count(det.productCode) as Number_Of_Items FROM Orders ord,OrderDetails det,Customers cust
	WHERE ord.orderNumber = det.orderNumber
    AND cust.customerNumber = ord.customerNumber
    AND cust.customerName REGEXP "Signal Gift Stores"
    GROUP BY ord.orderNumber
    ORDER BY count(det.productCode) DESC;
    
-- ++ The 'correct' answer that gives the same values ++ --
SELECT OrderDetails.orderNumber,count(*) FROM Orders,OrderDetails,Customers
	WHERE Orders.orderNumber = OrderDetails.orderNumber
    AND Orders.customerNumber = Customers.customerNumber
    AND customerName = "Signal Gift Stores"
    GROUP BY OrderDetails.orderNumber
    ORDER BY count(*) DESC;

-- What's the total value of orders received from Danish customers in krones (assume one Danish krone is .17 dollars).
-- Format to one decimal place.

SELECT format(sum(det.priceEach*det.quantityOrdered)/.17,1) as Krones FROM Customers cust,Orders ord,OrderDetails det -- dollars/conversion to dollars
	WHERE cust.customerNumber = ord.customerNumber
    AND ord.orderNumber = det.orderNumber
    AND cust.country REGEXP "Denmark|denmark";

-- What is the difference in total sales for Harley Davidson products ordered between 2004 and 2005? Format to two decimal places.

SELECT thingA.productName,format(five - four,2) as Difference FROM
	(SELECT productName,sum(quantityOrdered*priceEach) as five FROM Orders,OrderDetails,Products
		WHERE Orders.orderNumber = OrderDetails.orderNumber
        AND Products.productCode = OrderDetails.productCode
        AND year(orderDate) = 2005
        AND productName REGEXP "Harley Davidson|harley davidson"
        GROUP BY productName)
        as thingA,
    (SELECT productName,sum(quantityOrdered*priceEach) as four FROM Orders,OrderDetails,Products
		WHERE Orders.orderNumber = OrderDetails.orderNumber
        AND Products.productCode = OrderDetails.productCode
        AND year(orderDate) = 2004
        AND productName REGEXP "Harley Davidson|harley davidson"
        GROUP BY productName)
        as thingB
	GROUP BY productName;
    
-- ++ The correct answer ++ --

SELECT A.productName, FORMAT(sales2005 - sales2004,2) AS Difference FROM
	(SELECT Products.productName, sum(quantityOrdered*priceEach) AS sales2005 FROM Orders, OrderDetails, Products
		WHERE Orders.orderNumber = OrderDetails.orderNumber
		AND Products.productCode = OrderDetails.productCode
		AND YEAR(orderDate) = 2005
		AND Products.productName REGEXP 'Harley Davidson'
		GROUP BY Products.productName) AS A,
	(SELECT Products.productName, sum(quantityOrdered*priceEach) AS sales2004 FROM Orders, OrderDetails, Products
		WHERE Orders.orderNumber = OrderDetails.orderNumber
		AND Products.productCode = OrderDetails.productCode
		AND YEAR(orderDate) = 2004
		AND Products.productName REGEXP 'Harley Davidson'
		GROUP BY Products.productName) AS B
	GROUP BY productName;

-- Report the text description of product lines that have not appeared in an order.

SELECT textDescription FROM ProductLines
	WHERE productLine IN( -- NOT IN will give all productLines that have appeared in an order, a DIVIDE
		SELECT productLine FROM Products
			WHERE productCode NOT IN(
				SELECT productCode FROM OrderDetails
                )
		);