
/**
 * Class Person here.
 * An employee for the Payroll object.
 * 
 * @author Dr. Janine E. Aronson 
 * @version 20150908T
 */
public class Person
{
    // instance variables - replace the example below with your own
    private String name;  // the employee's name
    private double hours;  // hours worked
    private double payRate; // hourly pay rate

    /**
     * Constructor for objects of class Person
     */
    public Person(String name, double hours, double payRate)
    {
        // initialise instance variables
        // this.var refers to the field of THIS class.
        // name, hours, payRate refer to the parameters of the constructor method.
        this.name = name;
        this.hours = hours;
        this.payRate = payRate;
    }
    public double getEarnings(){
        double theEarnings;
        if(hours <= 40){
            theEarnings = hours*payRate;
        }else{
            theEarnings = ((hours-40)*.5 + hours*payRate);
        }
        return theEarnings;
    }
    /**
     * Not a mutator. S
     */
    public void printPayroll(){
        String tempName = name;
        tempName = tempName + "                            ";
        tempName = tempName.substring(0,26);
        System.out.println(name + " " + payRate + " " + hours + " $" + this.getEarnings());
        //you could pad the numbers by creating a new STring for each, and assignin some bullshit to it
    }










}
