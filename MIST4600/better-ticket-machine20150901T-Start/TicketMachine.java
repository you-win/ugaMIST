/**
 * 
 * ***** On, 20150830/20150831, Plans for 0901T: ******************************************
 * Students develop methods takeAction, printThreeTickets, 
 *   printFourTickets, 
 * .setPrice. sets the price. The password must be 9899 to change the price.
 *    public void setPrice(int newPrice, int password)  
 * doublePrice: Add a new method to double the price.
 * doublePrice with a password.
 * printBalance: Add a new method to print out the current value of “balance”, 
 *    with enough text to be understandable.
 *      
 * printNTickets for N=1,2,3,4,5,6 with error check
 * 
 * (then introduce while and for loops). printNTickets(int n)
 *  6 versions. 3 with ifs/if elses, 1 recursion, 2 loops. *  
 *  
 * Note the pattern - Add a mutator and an accessor for (almost) 
 *   each field/attribute/global variable.
 * 
 * Other potential modifications:
 * Create a supervisorPassword
 * Include a password as a field. Must use the supervisorPassword to run the new
 *   getPassword, setPassword, and use it in the methods that require a password.
 * 
 * Modified, 20150827, Dr. Janine Aronson
 * 
 * TicketMachine models a ticket machine that issues
 * flat-fare tickets.
 * The price of a ticket is specified via the constructor.
 * Instances will check to ensure that a user only enters
 * sensible amounts of money, and will only print a ticket
 * if enough money has been input.
 * 
 * @author David J. Barnes and Michael Kölling
 * @version 2011.07.31
 */
public class TicketMachine
{
    // The price of a ticket from this machine.
    private int price;
    // The amount of money entered by a customer so far.
    private int balance;
    // The total amount of money collected by this machine.
    private int total;
    // The ticket destination
    private String destination;
    // Total tickets sold:
    private int ticketCount;

    /**
     * Create a machine that issues tickets of the given price.
     */
    public TicketMachine(String pDestination, int cost)
    {
        destination = pDestination;
        price = cost;
        balance = 0;
        total = 0;
        ticketCount = 0; // should add to reset, and create a mutator and accessor.
    }

    // Accessor Methods:

    /**
     * @Return The price of a ticket.
     */
    public int getPrice()
    {
        return price;
    }

    /**
     * Return The amount of money already inserted for the
     * next ticket.
     */
    public int getBalance()
    {
        return balance;
    }

    /**
     * .getTotal.
     * Return The total amount of money collected for selling the tickets.
     */
    public int getTotal()
    {
        return total;
    }

    /**
     * @Return The destination of a ticket.
     */
    public String getDestination()
    {
        return destination;
    }

    // Some Mutator Methods:
    /**
     * .setPrice. sets the price
     * The password must be 9899 to change the price.
     * Think about it - implement it.
     */
    public void setPrice(int newPrice, int password)
    {
        if(password == 9899){
            if(newPrice >= 0)
            {
                price = newPrice;
            }else{
                System.out.println("*** Error *** Make the price >= 0, not " + newPrice + " !!! ***");
            }
        }else{
                System.out.println("*** Error *** Wrong Password !!! ***");
             }
        
    }

    
    /**
     * .takeAction. Takes one of three actions - a menu system.
     * 1 = print price
     * 2 = print destination
     * 3 = print ticket
     * This is a more complicated if - it uses if, else if, else if, else
     * I will create two versions.
     */
    public void takeAction(int number)
    {
        if(number == 1){
            System.out.println("The price is " + price + " cents.");
        }else if(number == 2){
            System.out.println("The destination is " + destination + ".");
        }else if(number == 3){
            printTicket();
        }else{
            System.out.println("WHY");
        }
    }
    
    /**
     * .setBalance. sets the price
     * 
     */
    public void setBalance(int newBalance)
    {
        if(newBalance >= 0)
        {
            balance = newBalance;
        }
        else
        {
            System.out.println("*** Error *** Make the balance >= 0, not " + newBalance + " !!! ***");
        }
    }

    /**
     * .setTotal. sets the total
     * 
     */
    public void setTotal(int newTotal)
    {
        if(newTotal >= 0)
        {
            total = newTotal;
        }
            else
        {
            System.out.println("*** Error *** Make the total >= 0, not " + newTotal + " !!! ***");
        }
    }

    /**
     * .setDestination. sets the destination
     * 
     */
    public void setDestination(String newDestination)
    {
        if(newDestination.trim().length() > 0)
        {
            destination = newDestination;
        }
            else
        {
            System.out.println("*** Error *** Make the destination nonempty !!! ***");
        }
    }

    /**
     * .resetMachine. Resets the machine to 0 including price
     */
    public void resetMachine(int password)
    {
        // Just use the three methods above
        setPrice(100,password);
        setBalance(0);
        setTotal(0);
        setDestination("Atlanta");
    }
    
    
    /**
     * Receive an amount of money from a customer.
     * Check that the amount is sensible.
     */
    public void insertMoney(int amount)
    {
        if(amount >= 0) {
            balance = balance + amount;
        }
        else {
            System.out.println("Use a positive amount (or zero) rather than: " +
                amount);
        }
    }

    /**
     * Print a ticket if enough money has been inserted, and
     * reduce the current balance by the ticket price. Print
     * an error message if more money is required.
     */
    public void printTicket()
    {
        if(balance >= price) 
        {
            // Update the ticket count
            ticketCount = ticketCount + 1;
            
            // Simulate the printing of a ticket.
            System.out.println("##################");
            System.out.println("# The BlueJ Line");
            System.out.println("# Ticket to " + destination);
            System.out.println("# Ticket Number " + ticketCount);
            System.out.println("# " + price + " cents.");
            System.out.println("##################");
            System.out.println();

            // Update the total collected with the price.
            total = total + price;
            // Reduce the balance by the prince.
            balance = balance - price;
        }
        else 
        {
            System.out.println("You must insert at least: " +
                (price - balance) + " more cents.");

        }
    }

    /**
     * Return the money in the balance.
     * The balance is cleared.
     */
    public int refundBalance()
    {
        int amountToRefund;
        amountToRefund = balance;
        balance = 0;
        return amountToRefund;
    }
    public void dumbPrintThreeTickets(){
        if(balance >= 3 * price){
            printTicket();
            printTicket();
            printTicket();
        }else{
            System.out.println("MEEP");
        }
    }
    public void printThreeTickets(int userInput1){
        int i = 0;
        while(i<userInput1){
            if(balance >= price){
                printTicket();
                i ++;
            }else{
                System.out.println("Why so poor");
                break;
            }
            //for(i;i<userInput1;i++){printTicket();}
            //do{printTicket();}while()
        }
    }
    public void doublePrice(int password){
        if(password == 9989){
            price = price*2;
        }else{
            System.out.println("WHY");
        }
    }
    public void dumbPrintNTicketsV01(int number){
        if(number < 1 || number > 6){
            System.out.println("You messed up!");
        }
        if(number == 1){
            printTicket();
        }else if(number == 2){
            printTicket();
            printTicket();
        }else if(number == 3){
            printTicket();
            printTicket();
        }
    }
    public void dumbPrintNTicketsV03(int number){
        if(number < 1 || number > 6){
            System.out.println("You messed up!");
        }
        if(number >= 1){
            printTicket();
        }
        if(number >= 2){
            printTicket();
        }
        if(number >= 3){
            printTicket();
        }
        if(number >= 4){
            printTicket();
        }
        if(number >= 5){
            printTicket();
        }
    }
    public void dumbPrintNTicketsV04(int number){ //recursive stuff
        if(number < 1){
            System.out.println("You messed up!");
        }
        if(number > 1){
            printTicket();
            dumbPrintNTicketsV04(number - 1);
        }else{
            printTicket();
            return;
        }
    }
    public void randomThingy(String thingy,int number){
        int i = 0;
        if(number >= 0){
            while(i <= number){
                if(i%2 == 0){
                    System.out.print(thingy);
                    i ++;
                }else{
                    System.out.println(thingy);
                    i ++;
                }
            }
        }else{
            System.out.println("Fuck you");
        }
    }
}
