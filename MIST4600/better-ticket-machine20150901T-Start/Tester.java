
/**
 * Write a description of class Tester here.
 * 
 * @author Dr. Janine E. Aronson 
 * @version 20150901T
 */
public class Tester
{
    // instance variables - replace the example below with your own
    private TicketMachine x;

    /**
     * Constructor for objects of class Tester
     */
    public Tester()
    {
        // initialise instance variables
        x = new TicketMachine("Atlanta", 100);
        x.insertMoney(300);
        x.printTicket();
        x.printTicket();
        int moneyBack = x.refundBalance();
        System.out.println("Refunded this amount " + moneyBack);
    }

}
