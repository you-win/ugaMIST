
/**
 * Class Payroll creates Person objects and prints out a header,
 * and tells the Person objects to print his or her payroll info.
 * 
 * @author Timothy Yuen
 * @version 20150910R
 */
public class Payroll{
    // instance variables - replace the example below with your own
    //private int x;
    private Person person01;
    private Person person02;
    private Person person03;
    private Person person04;
    private Person person05;
    Person[] listOPeeps = {person01,person02,person03,person04,person05};

    /**
     * Constructor for objects of class Payroll
     */
    public Payroll()
    {
        //initialise the instance variables
        person01 = new Person("Sterling Archer",40,7.25);
        person02 = new Person("Malorie Archer",32,20.00);
        person03 = new Person("Pam",51,9.5);
        person04 = new Person("Chris Carpentier",25,17.25);
        person05 = new Person("Ben Walton",42,18.95);
        //Person[] listOPeeps = {person01,person02,person03,person04,person05};
    }
    /**
     * .printPayroll.
     * prints a header, tells each Person object to print its payroll line,
     * then prints a blank line.total payroll for the week.
     * Also, compute the payroll for the week.
     * Just like ticketcount in the better ticket machine.
     */
    public void printPayroll(){
        System.out.println("Name        Payrate     Hours       Pay");
        double totalPayroll;
        totalPayroll = 0.;
        /*int i = 0;
        while(i<=5){
            listOPeeps[i].printPayroll();
            totalPayroll = totalPayroll + listOPeeps[i].getEarnings();
            i++;
        }*/
        person01.printPayroll();
        totalPayroll = totalPayroll + person01.getEarnings();
        person02.printPayroll();
        totalPayroll = totalPayroll + person02.getEarnings();
        person03.printPayroll();
        totalPayroll = totalPayroll + person03.getEarnings();
        person04.printPayroll();
        totalPayroll = totalPayroll + person04.getEarnings();
        person05.printPayroll();
        totalPayroll = totalPayroll + person05.getEarnings();
        
        System.out.println();
        System.out.println("Total Weekly Payroll:   $" + totalPayroll);
        System.out.println();
    }
}
