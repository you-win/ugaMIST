-- Find all orders where it took more than a year for the customer to pay for the order
SELECT orderNumber,datediff(paymentDate,orderDate) as DaysTakenToPay, amount as AmountInDollars FROM Payments pay,Customers cust,Orders ord
	WHERE pay.customerNumber = cust.customerNumber
    AND cust.customerNumber = ord.customerNumber
    AND datediff(paymentDate,orderDate) > 365
    GROUP BY orderNumber,amount