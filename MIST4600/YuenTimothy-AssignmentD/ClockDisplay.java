/**
 * Name: Timothy Yuen 9/16/2015
 * Code Status: Before the alarm was added, the code was clean enough. After the alarm was added, too many things were added
 *              and then taken out, resulting in some messiness in the code.
 *              I looked up on StackOverflow on how to compare two strings and found a built-in function .equals() that allowed
 *              me to compare the displayString and alarmTime without just verifying that both were indeed just Strings.
 *              I think an alarm could be made by creating a boolean that turns true if the value of hours/minutes/seconds
 *              becomes greater than a certain value, thus activating the alarm.
 * 
 * Assignment D - see the Assignment writeup.
 * 
 * The ClockDisplay class implements a digital clock display for a
 * European-style 24 hour clock. The clock shows hours and minutes. The 
 * range of the clock is 00:00 (midnight) to 23:59 (one minute before 
 * midnight).
 * 
 * The clock display receives "ticks" (via the timeTick method) every minute
 * and reacts by incrementing the display. This is done in the usual clock
 * fashion: the hour increments when the minutes roll over to zero.
 * 
 * @author Michael Kolling and David J. Barnes
 * @version 2006.03.30
 */
public class ClockDisplay
{
    private NumberDisplay hours;
    private NumberDisplay minutes;
    private String displayString;    // simulates the actual display
    
    private NumberDisplay seconds;
    
    private NumberDisplay alarmSeconds;
    private NumberDisplay alarmMinutes;
    private NumberDisplay alarmHours;
    private String alarmTime;
    private boolean alarmSwitch; //is the alarm on or off
    
    /**
     * Constructor for ClockDisplay objects. This constructor 
     * creates a new clock set at 00:00.
     */
    public ClockDisplay()
    {
        hours = new NumberDisplay(24);
        minutes = new NumberDisplay(60);
        seconds = new NumberDisplay(0);
        updateDisplay();
    }

    /**
     * Constructor for ClockDisplay objects. This constructor
     * creates a new clock set at the time specified by the 
     * parameters.
     */
    public ClockDisplay(int hour, int minute)
    {
        hours = new NumberDisplay(24);
        minutes = new NumberDisplay(60);
        seconds = new NumberDisplay(0);
        setTime(hour, minute,0);
    }
    
    public ClockDisplay(int hour, int minute, int second) //the only constructor that allows for an alarm because eh
    {
        hours = new NumberDisplay(24);
        minutes = new NumberDisplay(60);
        seconds = new NumberDisplay(60);
        setTime(hour,minute,second);
        alarmSwitch = false;
        alarmHours = new NumberDisplay(24);
        alarmMinutes = new NumberDisplay(60);
        alarmSeconds = new NumberDisplay(60);
        alarmHours.setValue(0);
        alarmMinutes.setValue(0);
        alarmSeconds.setValue(0);
        alarmTime = "";
    }
    
    public void setAlarm(int hour,int minute,int second)
    {
        alarmHours.setValue(hour);
        alarmMinutes.setValue(minute);
        alarmSeconds.setValue(second);
        alarmTime = alarmHours.getDisplayValue() + ":" + 
                        alarmMinutes.getDisplayValue() + ":" +
                        alarmSeconds.getDisplayValue();
        System.out.println("Alarm set to " + alarmTime + ".");
    }
    
    public void setAlarmSwitch(boolean x)
    {
        alarmSwitch = x;
        if(x == true)
        {
            System.out.println("Alarm on");
        }
        else
        {
            System.out.println("Alarm off");
        }
    }
    
    public String getAlarm()
    {
        return alarmTime;
    }

    /**
     * This method should get called once every minute - it makes
     * the clock display go one second forward.
     */
    public void timeTick()
    {
        seconds.increment();
        if(seconds.getValue() == 0)
        {  // it just rolled over!
            minutes.increment();
            if(minutes.getValue() == 0)
            {
                hours.increment();
            }
        }
        updateDisplay();
    }
    
    public void timeTickMinute()
    {
        minutes.increment();
        if(minutes.getValue() == 0)
        {
            hours.increment();
        }
        updateDisplay();
    }
    
    public void timeTickHour()
    {
        hours.increment();
        updateDisplay();
    }

    /**
     * Set the time of the display to the specified hour and
     * minute.
     */
    public void setTime(int hour, int minute, int second)
    {
        hours.setValue(hour);
        minutes.setValue(minute);
        seconds.setValue(second);
        updateDisplay();
    }

    /**
     * Return the current time of this display in the format HH:MM.
     */
    public String getTime()
    {
        return displayString;
    }
    
    /**
     * Update the internal String that represents the display.
     */
    private void updateDisplay()
    {
        displayString = hours.getDisplayValue() + ":" + 
                        minutes.getDisplayValue() + ":" +
                        seconds.getDisplayValue();
        // added output to terminal window
        //System.out.println(displayString); // This prints out the displayString with every update.
        if(alarmSwitch == true)
        {
            if(alarmTime.equals(displayString)) //doing it with a comparison of strings
            {
                int i = 1;
                System.out.println(displayString);
                while(i < 5)
                {
                    if(i%2 == 0)
                    {
                        System.out.println("BEEP");
                    }
                    else
                    {
                        System.out.println("BOOP");
                    }
                    i ++;
                }
            }
            else
            {
                System.out.println(displayString);
            }
        }
        else
        {
            System.out.println(displayString);
        }
    }
}
