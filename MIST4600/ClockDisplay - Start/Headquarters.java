
/**
 * Class Headquarters
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Headquarters{
    // instance variables - replace the example below with your own
    private ClockDisplay atlanta;
    private ClockDisplay chicago;
    private ClockDisplay losAngeles;
    private ClockDisplay greenwich;

    /**
     * Constructor for objects of class Headquarters
     */
    public Headquarters(){
        atlanta = new ClockDisplay(0,0);
        chicago = new ClockDisplay(23,0);
        losAngeles = new ClockDisplay(21,0);
        greenwich = new ClockDisplay(6,0);
        
        atlanta.setName("Atlanta");
        chicago.setName("Chicago");
        losAngeles.setName("Los Angeles");
        greenwich.setName("Greenwich");
    }
    public void setTime(int clockNumber,int newHour,int newMinute){
        if(clockNumber < 1 || clockNumber > 4){
            return;
        }
        if(clockNumber == 1){
            atlanta.printTime();
        }else if(clockNumber == 2){
            chicago.printTime();
        }else if(clockNumber == 3){
            losAngeles.printTime();
        }else if(clockNumber == 4){
            greenwich.printTime();
        }
    }
    /**
     * .timeTick. advances the time by one minute in all the clocks
     */
    public void timeTick(){
        atlanta.timeTick();
        chicago.timeTick();
        losAngeles.timeTick();
        greenwich.timeTick();
    }
    /**
     * .displayAllTime. display the time on all the clocks
     */
    public void displayAllTime(){
        atlanta.printTime();
        chicago.printTime();
        losAngeles.printTime();
        greenwich.printTime();
    }
    public void displayTime(int clockNumber){
        
    }
}
