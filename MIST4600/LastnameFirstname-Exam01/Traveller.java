
/**
 * The Traveller class is used to create a Passenger object. This object
 * has a first and last name and ID that are provided.
 * @Nikhil Srinivasan 
 * @2014.02.08
 * 
 * Comments:
 * 
 * 
 */
public class Traveller
{
    // instance variables - Traveller First name, Last name, ID
    private String tFname;
    private String tLname;
    private int tId;

    /**
     * Constructor for objects of class Traveller with fname and lname
     */
    public Traveller(String pFname, String pLname, int pId)
    {
        // initialise instance variables
        setFname(pFname);
        setLname(pLname);
        setId(pId);
    }

   
    /**
     * Accessor for tFname
     */
    public String getFname()
    {
        return tFname;
    }
    
    /**
     * Accessor for tLname
     */
    public String getLname()
    {
        return tLname;
    }
    
    /**
     * Set tfname after non-black check
     */
    public void setFname(String pFname)
    {
            tFname = pFname;
    }
    
    /**
     * Set tLname after non-blank check
     */
    public void setLname(String pLname)
    {
            tLname = pLname;
     }
    
    /**
     * Set tId after greater than zero check
     */
    public void setId(int pId)
    {
        if(pId > 0){
            tId = pId;
        }
    }
    
    /**
     * Check the ID of the traveller if he is Wizard or Muggle
     */
    public boolean checkIfWizard(int pId)
    {
        boolean trueOrFalse = false;
        if(pId <=1000 && pId > 0){
            trueOrFalse = true;
        }
        return trueOrFalse;
    }

     /**
     * Check the ID of the traveller against Potter's ID of 232
     */
    public boolean checkIfHarry(int pId)
    {
        boolean trueOrFalse = false;
        if(pId == 232){
            trueOrFalse = true;
        }
        return trueOrFalse;
    }
    
}
