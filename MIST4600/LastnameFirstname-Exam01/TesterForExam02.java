
/**
 * This tester tests the blank and invalid inputs for the price, id, and names of the passenger
 * 
 * 
 *  You must insert at least: 3 more pounds.
 *  Please input a first name of the travelling individual.
 *  Please input a last name of the travelling individual.
 *  Please input a non-zero value for ID of the travelling individual.
 * 
 * 
 * @author Nikhil Srinivasan
 * @version 2014.02.08
 */
public class TesterForExam02
{
    // instance variables - replace the example below with your own
    private TicketMachine x;
    private Traveller y;
    /**
     * Constructor for objects of class TesterForExam02
     */
    public TesterForExam02()
    {
        // initialise instance variables
        x = new TicketMachine("Manchester", 5);
        x.insertMoney(2);
        System.out.println();
        x.printTicket("Harry","Potter",232);
        x.insertMoney(50);
        
        y = new Traveller("Harry","Potter",0);
        
        System.out.println();
        System.out.println("*****For Part B: Expect to see 2 Error messages, one for not enough money and 1 for 0 input value*****");
        
        
        
    }

}
